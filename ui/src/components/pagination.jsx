import React from 'react';
import '../styles/pagination.css';

const Pagination = ({itemsPerPage, totalItems, paginate, currentPage}) => {
    const pageNumbers = [];

    for(let i = 1; i <= Math.ceil(totalItems / itemsPerPage); i++){
        pageNumbers.push(i);
    }

    return (
        <div className="bd-example">
            <nav>
                <ul className="pagination">
                    { currentPage > 1 ? (
                        <li className="page-item">
                            <a href="#" onClick={() => paginate(currentPage--)} className="page-link">
                                    Previous
                            </a>
                        </li>
                    ): null}
                    
                    {pageNumbers.map(number => (
                        
                        <li key={number} className={currentPage === number ? "page-item active" : "page-item"}>
                            
                            <a href="#" onClick={() => paginate(number)} className="page-link">
                                {number}
                            </a>
                        </li>
                    ))}

                    { currentPage < pageNumbers.length ? (
                        <li className="page-item">
                            <a href="#" onClick={() => paginate(currentPage++)} className="page-link">
                                    Next
                            </a>
                        </li>
                    ): null}
                </ul>
            </nav>
        </div>
        )
}

export default Pagination;